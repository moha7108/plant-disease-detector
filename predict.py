import modelCNN
import torch


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(f'{device} detected, using {device} to run model')

out, idx = modelCNN.predictPD('./test_images/test_image_black_rot.png', './checkpoint_2.pt', './imagenet_class_index.json', device)

# print(idx)
print(out)
