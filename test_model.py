
import numpy as np
import matplotlib.pyplot as plt

import torch
from torch import nn
from torch import optim
import torchvision
from torchvision import datasets, models, transforms

import modelCNN



def load_data(img_data_root, batch_size=4):
    '''
    input_args
        img_data_root: is the root directory of the image containing the training('train'), validation('valid'), and test('test') image directories
        batch_size: size of the batches for training the model

    output

    '''

    # Tranformations for processing and loading train/test data
    process_transform = transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
                        ])
    augment_transform = transforms.Compose([
                        transforms.RandomResizedCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                        ])

    test_data = datasets.ImageFolder(img_data_root, transform=process_transform)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=True)

    num_classes = len(test_data)

    return test_loader





def test_model(testloader, load_model_path, device):

    model, _, _, _ = modelCNN.load_model(load_model_path, device)
    # model = modelCNN.model_architecture(num_classes)
    # model.load_state_dict(torch.load(load_model_path)['state_dict'])
    model.to(device)


    model.eval()

    correct=0
    total=0

    with torch.no_grad():
        for (image, label) in testloader:

            image, label = image.to(device), label.to(device)
            outputs = model(image)
            _, predicted = torch.max(outputs.data, 1)
            # print(f'this is predicted: {predicted}')
            # print(f'this is label: {label}')
            total += label.size(0)
            correct += (predicted == label).sum().item()

    print('Accuracy of the network on the test images: %d %%' % (
    100 * correct / total))

    return correct, total


if __name__ == '__main__':

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'{device} detected, using {device} to run model')


    # test_loader = load_data('./data/GoogleTestImages') #haahaha accuracy was so low haha we cant show this result

    _, loader = modelCNN.load_data('./data')
    test_loader = loader['test']

    test_model(test_loader, './checkpoint_2.pt',device)
