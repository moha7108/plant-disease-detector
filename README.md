# Plant Disease Identifier (aka PDNet)

Transfer Learning, pretrained model: Resnet18  + custom classifier layers

Anaconda: environment.yml

pip: pip_requirements.txt

Deeplearning framework: Pytorch

Application framework: Flask

Dataset: https://www.kaggle.com/saroz014/plant-diseases

Project Description: This project is a tool for growers to identify common plant diseases.


## To Run server locally
*** on commandline ***
export FLASK_APP=application.py
export FLASK_ENV=development
flask run

Make sure to have all conda and pip dependencies (environment.yml and pip_requirements.txt)

## Deloped to server

***Work in progress please come back later***
