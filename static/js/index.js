// js for video feed if we need...

const video = document.getElementById('video');

//Requesting permission from chrome to access webcam
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
//Fuction which starts streaming og webcam data and creates stream object
function startVideo() {
    console.log("access");
    navigator.getUserMedia(
        {
            video: {}
        },
        stream => video.srcObject = stream,
        err => console.error(err)
    )
}

//Load models in a promise, then start video
startVideo()