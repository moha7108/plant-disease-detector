import requests
import re
from bs4 import BeautifulSoup

###
# Webscraps URL and returns the HTML for the first paragraph of the disease
# query should be result of disease model. If no disease, return Healthy plant!
# Optional search query params if needed
# URL should have a trailing /
###
def scraper(url='https://www.planetnatural.com/pest-problem-solver/plant-disease/', query='apple-scab',params=None):
    base_url = url
    q = query.replace(' ','-') 
    if query == None:
        return '<h1> Healthy plant! </h1>'
    response = requests.get(f'{url}{q}')
    if params:
        params = params
        response = requests.get(url, params=params)


    soup = BeautifulSoup(response.content, 'html.parser')
    main = soup.find(id='main')
    if main:
        post_entry = main.find(class_='post-entry')
        description = post_entry.find('p')

        return description
    return ''

    